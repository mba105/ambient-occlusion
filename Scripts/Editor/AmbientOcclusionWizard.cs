using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

// Based on script by Adrian Myers: http://adrianswall.com/?p=28
public class AmbientOcclusionWizard : ScriptableWizard
{
	#region wizard interface
	[MenuItem("Window/Vertex Ambient Occlusion")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard<AmbientOcclusionWizard>("Vertex Ambient Occlusion");
	}

	private void OnGUI()
	{
		EditorGUILayout.BeginVertical();

		if (errorString != "")
		{
			EditorGUILayout.LabelField(
				new GUIContent(errorString, helpString));

			EditorGUILayout.Separator();
		}

		ao.Samples = EditorGUILayout.IntField(
			new GUIContent("Samples", "Number of raycasts to use per vertex."),
			ao.Samples);

		ao.MaxDistance = EditorGUILayout.FloatField(
			new GUIContent("Max Distance", "Length of raycast from vertex - objects outside this range are ignored."),
			ao.MaxDistance);

		ao.MinDistance = EditorGUILayout.FloatField(
			new GUIContent("Min Distance", "Objects closer than this distance from the vertex are ignored."),
			ao.MinDistance);

		ao.OffsetDistance = EditorGUILayout.FloatField(
			new GUIContent("Offset Distance", "Start distance of raycast from vertex - objects closer than this distance from the vertex are ignored."),
			ao.OffsetDistance);

		ao.TestConcaveSelf = EditorGUILayout.Toggle(
			new GUIContent("Test Concave Self", "Enable this if geometry has concave vertex angles. Several extra raycasts (2 or more, but usually 2) per vertex determine if ray ends inside the object."),
			ao.TestConcaveSelf);

		ao.TestParallelOther = EditorGUILayout.Toggle(
			new GUIContent("Test Parallel Other", "Enable to catch different objects with adjacent parallel faces. One extra raycast per vertex."),
			ao.TestParallelOther);

		ao.TestConcaveOther = EditorGUILayout.Toggle(
			new GUIContent("Test Concave Other", "Enable to catch different objects forming a concave angle with this one. One extra raycast per vertex."),
			ao.TestConcaveOther);

		ao.TestInsideOther = EditorGUILayout.Toggle(
			new GUIContent("Test Inside Other", "Enable to catch when vertices are inside those of another object. One extra raycast per vertex."),
			ao.TestInsideOther);

		GUILayout.FlexibleSpace();

		EditorGUILayout.BeginHorizontal();

		GUILayout.FlexibleSpace();

		if (GUILayout.Button(
			new GUIContent("Bake", "Bake ambient occlusion in selected objects.")))
		{
			if (isValid)
			{
				Bake();

				Close();
			}
		}

		EditorGUILayout.Separator();

		if (GUILayout.Button(
			new GUIContent("Reset", "Resets all settings to their defaults.")))
		{
			Reset();
		}

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Separator();

		EditorGUILayout.EndVertical();
	}

	private void OnWizardUpdate()
	{
		ValidateSelection();
	}
	#endregion

	#region selection
	private void OnSelectionChange()
	{
		ValidateSelection();
	}

	private bool IsSelectionValid()
	{
		Transform[] selection = Selection.transforms;
		return selection.Length != 0;
	}

	private bool IsSelectionUseful()
	{
		Transform[] selection = Selection.transforms;

		int meshFilters = selection.Count(t => t.gameObject.GetComponentInChildren<MeshFilter>() != null);

		return meshFilters != 0;
	}

	private void ValidateSelection()
	{
		bool validSelection = IsSelectionValid();
		bool usefulSelection = IsSelectionUseful();

		if (!validSelection)
		{
			errorString = "No objects selected.";
			helpString = "Select one or more scene objects containing MeshFilters.";
		}

		else if (!usefulSelection)
		{
			errorString = "No MeshFilters in selected objects.";
			helpString = "Select one or more scene objects containing MeshFilters.";
		}
		else
		{
			errorString = "";
			helpString = "";
		}

		isValid = IsSelectionValid() && IsSelectionUseful();
	}

	#endregion

	static private AmbientOcclusion ao = new AmbientOcclusion();

	private void Reset()
	{
		ao = new AmbientOcclusion();
	}

	private void Bake()
	{
		Transform[] selection = Selection.transforms;

		for (int i = 0; i != selection.Length; ++i)
		{
			EditorUtility.DisplayProgressBar(
				"Baking Ambient Occlusion",
				"Object " + (i + 1) + " / " + selection.Length,
				(float) i / (float) selection.Length);

			// Bake will find all mesh filters in the object and its children.
			ao.Bake(selection[i].gameObject);
		}

		EditorUtility.ClearProgressBar();
	}
}