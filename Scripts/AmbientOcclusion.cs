using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Zip;

// Based on script by Adrian Myers: http://adrianswall.com/?p=28
public class AmbientOcclusion
{
	public int Samples { get; set; }

	public float MaxDistance { get; set; }
	public float MinDistance { get; set; }
	public float OffsetDistance { get; set; }

	public bool TestConcaveSelf { get; set; }
	public bool TestParallelOther { get; set; }
	public bool TestConcaveOther { get; set; }
	public bool TestInsideOther { get; set; }

	public AmbientOcclusion()
	{
		Samples = 512;
		MaxDistance = 1.5f;
		MinDistance = 0.05f;
		OffsetDistance = 0.01f;

		TestConcaveSelf = true;
		TestParallelOther = true;
		TestConcaveOther = true;
		TestInsideOther = true;
	}

	public void Bake(GameObject gameObject)
	{
		MeshFilter[] meshFilters = gameObject.GetComponentsInChildren<MeshFilter>();

		// Calculate ambient occlusion.
		RaycastHit hit;

		foreach (MeshFilter meshFilter in meshFilters)
		{
			// This leaks meshes into the scene (sharedMeshes with no users).
			// Unity will whine about them on save, and clear them up on Editor shutdown.
			// Unfortunately there's not much that can be done about it.
			Mesh mesh = Mesh.Instantiate(meshFilter.sharedMesh) as Mesh;
			meshFilter.mesh = mesh;

			Collider collider = meshFilter.collider;

			// Transform vertices / normals to world space.
			Vector3[] vertices = mesh.vertices;
			vertices = vertices.Select(v => meshFilter.transform.TransformPoint(v)).ToArray();

			Vector3[] normals = mesh.normals;
			normals = normals.Select(n => meshFilter.transform.TransformDirection(n)).ToArray();

			float[] ao = new float[vertices.Length];

			for (int i = 0; i != vertices.Length; ++i)
			{
				float occlusion = 0.0f;

				for (int j = 0; j != Samples; ++j)
				{
					// Get random ray in hemisphere.
					Vector3 rayDir = UnityEngine.Random.onUnitSphere;
					rayDir.y = Mathf.Abs(rayDir.y);

					// Transform ray to world space.
					Quaternion worldRayRot = Quaternion.FromToRotation(Vector3.up, normals[i]);
					Vector3 worldRayDir = (worldRayRot * rayDir).normalized;

					Vector3 offset = worldRayDir * OffsetDistance;
					Vector3 reflectedOffset = Vector3.Reflect(-worldRayDir, normals[i]) * OffsetDistance;

					Vector3 start = vertices[i];
					Vector3 end = vertices[i] + worldRayDir * MaxDistance;

					if (Physics.Linecast(start + offset, end, out hit))
					{
						// Standard catch-all pass 1.
						// Catch geometry facing vertex.
						float distance = hit.distance;
						if (distance - OffsetDistance > MinDistance)
						{
							++occlusion;
							continue;
						}
					}
					else if (Physics.Linecast(end, start + offset, out hit))
					{
						// Standard catch-all pass 2.
						// Catch geometry facing away from vertex.
						float distance = MaxDistance - hit.distance;
						if (distance - OffsetDistance > MinDistance)
						{
							++occlusion;
							continue;
						}
					}

					if (TestConcaveSelf && PointInMesh.Test(end, collider))
					{
						// Special case 1.
						// Catch concave self-occlusion.
						++occlusion;
						continue;
					}
					if (TestParallelOther && Physics.Linecast(start - offset, start + offset, out hit))
					{
						// Special case 2.
						// Catch parallel touching objects.
						if (!GameObjectRelations.DirectRelationOrEqual(
								hit.collider.gameObject, meshFilter.gameObject))
						{
							if (Mathf.Approximately(
									Vector3.Angle(hit.normal, normals[i]), 180.0f))
							{
								++occlusion;
								continue;
							}
						}
					}
					if (TestConcaveOther && Physics.Linecast(start + reflectedOffset, end, out hit))
					{
						// Special case 3.
						// Catch concave touching objects.
						if (!GameObjectRelations.DirectRelationOrEqual(
								hit.collider.gameObject, meshFilter.gameObject))
						{
							++occlusion;
							continue;
						}
					}

					if (TestInsideOther && Physics.Linecast(end, start + offset, out hit))
					{
						// Special case 4.
						// Catch vertex inside other object (within ray range).
						if (!GameObjectRelations.DirectRelationOrEqual(
								hit.collider.gameObject, meshFilter.gameObject))
						{
							++occlusion;
							continue;
						}
					}
				}

				// Normalize and invert to actual occlusion value
				ao[i] = Mathf.Clamp01(1.0f - (occlusion / Samples));
			}

			// Store occlusion value in mesh color alpha.
			// TODO: other options (uv etc.)?

			Color[] colors = mesh.colors;
			colors = colors.Zip(ao, (c, o) => { c.a = o; return c; }).ToArray();
			mesh.colors = colors;
		}
	}
}