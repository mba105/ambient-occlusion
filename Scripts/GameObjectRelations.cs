using UnityEngine;
using System.Collections;

public class GameObjectRelations
{
	// TODO: proper testing!

	static public bool DifferentHierarchy(GameObject go1, GameObject go2)
	{
		return go1.transform.root != go2.transform.root;
	}

	static public bool SameHierarchy(GameObject go1, GameObject go2)
	{
		return go1.transform.root == go2.transform.root;
	}

	static public bool Sibling(GameObject go1, GameObject go2)
	{
		return go1.transform.parent == go2.transform.parent;
	}

	static public bool Cousin(GameObject go1, GameObject go2)
	{
		return Sibling(go1.transform.parent.gameObject, go2.transform.parent.gameObject);
	}

	static public bool DirectAncestor(GameObject ancestor, GameObject child)
	{
		Transform current = child.transform.parent;

		while (current != null)
		{
			if (current.gameObject == ancestor)
				return true;

			current = current.parent;
		}

		return false;
	}

	static public bool Equal(GameObject go1, GameObject go2)
	{
		return go1 == go2;
	}

	static public bool DirectRelation(GameObject go1, GameObject go2)
	{
		return DirectAncestor(go1, go2) || DirectAncestor(go2, go1);
	}

	static public bool DirectRelationOrEqual(GameObject go1, GameObject go2)
	{
		return Equal(go1, go2) || DirectRelation(go1, go2);
	}
}
