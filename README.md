# Ambient Occlusion #

![image1.png](https://bitbucket.org/repo/dg4Gj5/images/1175838595-image1.png)

Ambient occlusion baking wizard for Unity3D using Physics.LineCast, based on script by Adrian Myers: http://adrianswall.com/?p=28